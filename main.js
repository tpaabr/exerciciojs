const $ = document

let addEl = function(){
    let pai = $.querySelector(".lista");
    let quantidadeFilhos = pai.children.length;
    let filho = $.createElement("li");
    filho.innerText = quantidadeFilhos + 1;
    pai.appendChild(filho);

}

let removeEl = function(){
    let pai = $.querySelector(".lista");
    let listaFilhos = pai.children;
    let ultimoFilho = listaFilhos[(listaFilhos.length) - 1];
    ultimoFilho.remove();
}

let unList = $.querySelector(".lista");

let removerAoClicar = function(evento){
    unList.removeChild(evento.target)
}

let addOnclick = function(){
    addEl();
}
let removeOnclick = function(){
    removeEl();
}

let butAdd = $.querySelector(".add")
let butRemove = $.querySelector(".remove")

butAdd.addEventListener("click", addOnclick);
butRemove.addEventListener("click", removeOnclick);
unList.addEventListener("click", removerAoClicar);

